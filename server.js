const express = require('express');
const _ = require('lodash');
const app = express();
const port = 4000;
const cors=require('cors');

const agency = require("./agency");
const region = require("./region");
const station = require("./station");
const metrics = require("./metrics");
const values = require("./values");

app.use(express.json());
app.use(cors());// server will respond to any domain

app.get('/', (req, res) => res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' }));

// GET: List all agencies
app.get('/weather/v1/agency', (req, res) => res.send(
        { agency: _.filter(agency, function(o) {
            return _.map({}, function(v,k) {
                return _.includes(o[k],v)
            }).reduce(function(isMatch, keyValueMatch){
                return isMatch && keyValueMatch
            }, true);
            })
        }
    ));

// GET: lists all the regions within an agency
app.get('/weather/v1/agency/:id/region', (req, res) => res.send(
        { region: _.filter(region, function(o) {
                return _.map({}, function(v,k) {
                    return _.includes(o[k],v)
                }).reduce(function(isMatch, keyValueMatch){
                    return isMatch && keyValueMatch
                }, true);
            })
        }
    ));

// GET: retrieves the station by the provided id
app.get('/weather/v1/station/:id', (req, res) => res.json(
        { station: _.filter(station, function(o) {
                return _.map({}, function(v,k) {
                    return _.includes(o[k],v)
                }).reduce(function(isMatch, keyValueMatch){
                    return isMatch && keyValueMatch
                }, true);
            }) }
    ));

// GET: list of all supported metrics
app.get('/weather/v1/metrics', (req, res) => res.json({ metrics: metrics }));

// GET: Retrieves the values of a certain metric in the specified date range.
app.get('/weather/v1/metric/:id/values', (req, res) => res.json({ values: values }));


app.listen(port, () =>
    console.log(`Simulated backend listening on port ${port}!`)
);
