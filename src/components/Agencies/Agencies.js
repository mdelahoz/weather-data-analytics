import React, { Component } from 'react';

import Panel from '../Panel/Panel';
import Form from "react-bootstrap/Form";

const Agency = ({ id, name }) => (
    <option value={id}>{name}</option>
);

class Agencies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            agencyId: null,
        };
        this.handleChange= this.handleChange.bind(this);
    }

    handleChange(event) {
        console.log('on change event', event.target.value);
        let selectedId = event.target.value;
        this.setState({agencyId: selectedId});
    }

    render() {
        let panelContent;


        if (this.props.loading) {
            panelContent = (
                <p>Loading...</p>
            );
        } else {
            if (this.props.error) {
                panelContent = (
                    <p>Something went wrong while fetching the data: <code>{this.props.error}</code></p>
                );
            } else {
                if(this.props.data.agency !== undefined){
                    panelContent = (
                        <Form>
                        <Form.Group>
                            <Form.Control as="select" onChange={this.handleChange}>
                            <option>Agency</option>
                            {this.props.data.agency.map((agency,index) => (
                                <Agency
                                key={index}
                                id={agency.id}
                                name={agency.name}
                                />
                            ))}
                            </Form.Control>
                        </Form.Group>
                        </Form>
                    );
                }
            }
        }

        return (
            <Panel>
                {panelContent}
            </Panel>
        );
    }
}

export default Agencies;
