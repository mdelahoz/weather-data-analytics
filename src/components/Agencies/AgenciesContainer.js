import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions as agenciesActions } from '../../ducks/agencies';
import Agencies from './Agencies';

class AgenciesContainer extends Component {
    componentDidMount() {
        this.props.fetchAgencies();
    }

    render() {
        return (
            <Agencies {...this.props.agencies} />
        );
    }
}

const mapStateToProps = (state) => ({
    agencies: state.agencies,
});

const mapDispatchToProps = (dispatch) => ({
    fetchAgencies: () => dispatch(agenciesActions.fetchAgencies()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AgenciesContainer);