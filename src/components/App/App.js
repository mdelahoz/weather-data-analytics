import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import moment from 'moment';
import AgenciesContainer from '../Agencies/AgenciesContainer';
import RegionsContainer from '../Regions/RegionsContainer';
import MetricsContainer from '../Metrics/MetricsContainer';
import StationsContainer from '../Stations/StationsContainer';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
        };
    }


    render() {
        let start = this.state.startDate.format('YYYY-MM-DD');
        let end = this.state.endDate.format('YYYY-MM-DD');
        let label = start + ' - ' + end;
        if (start === end) {
            label = start;
            console.log(label);
        }
        return (
            <div className="App">

                <header className="jumbotron" >

                    <h1 className="App-title">Weather App </h1>

                </header>

                <Container>
                    {/*<Form>*/}
                        <Row>
                            <Col>
                                <AgenciesContainer/>
                            </Col>
                            <Col>
                                <RegionsContainer/>
                            </Col>
                            <Col>
                                <MetricsContainer/>
                            </Col>
                        </Row>
                    {/*</Form>*/}
                    {/*<Form>*/}
                        <Row>
                            {/*<Col className='block-example border border-dark'>*/}
                                <StationsContainer/>
                            {/*</Col>*/}
                            {/*<Col></Col>*/}
                        </Row>
                    {/*</Form>*/}
                </Container>
            </div>

        );



    }

}


export default App;