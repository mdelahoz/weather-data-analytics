import React, { Component } from 'react';

import Panel from '../Panel/Panel';
import Form from "react-bootstrap/Form";


const Metric = ({ id, name }) => (
    <option value={id}>{name}</option>
);

class Metrics extends Component {
    render() {
        let panelContent;


        if (this.props.loading) {
            panelContent = (
                <p>Loading...</p>
            );
        } else {
            if (this.props.error) {
                panelContent = (
                    <p>Something went wrong while fetching the data: <code>{this.props.error}</code></p>
                );
            } else {
                console.log(this.props);
                if(this.props.data.metrics !== undefined){
                    panelContent = (
                        <Form.Group>
                            <Form.Control as="select" >
                                <option>Metrics</option>
                                {this.props.data.metrics.map((metric,index) => (
                                    <Metric
                                        key={index}
                                        id={metric.id}
                                        name={metric.name}
                                    />
                                ))}
                            </Form.Control>
                        </Form.Group>
                    );
                }
            }
        }

        return (
            <Panel>
                {panelContent}
            </Panel>
        );
    }
}

export default Metrics;
