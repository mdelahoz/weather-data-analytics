import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions as metricsActions } from '../../ducks/metrics';
import Metrics from './Metrics';

class MetricsContainer extends Component {
    componentDidMount() {
        this.props.fetchMetrics();
    }

    render() {
        return (
            <Metrics {...this.props.metrics} />
        );
    }
}

const mapStateToProps = (state) => ({
    metrics: state.metrics,
});

const mapDispatchToProps = (dispatch) => ({
    fetchMetrics: () => dispatch(metricsActions.fetchMetrics()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MetricsContainer);