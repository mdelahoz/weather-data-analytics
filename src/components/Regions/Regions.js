import React, { Component } from 'react';

import Panel from '../Panel/Panel';
import Form from "react-bootstrap/Form";


const Region = ({ id, name }) => (
    <option value={id}>{name}</option>
);

class Regions extends Component {
    render() {
        let panelContent;


        if (this.props.loading) {
            panelContent = (
                <p>Loading...</p>
            );
        } else {
            if (this.props.error) {
                panelContent = (
                    <p>Something went wrong while fetching the data: <code>{this.props.error}</code></p>
                );
            } else {
                if(this.props.data.region !== undefined){
                    panelContent = (
                        <Form.Group>
                            <Form.Control as="select" >
                                <option>Region</option>
                                {this.props.data.region.map((region,index) => (
                                    <Region
                                        key={index}
                                        id={region.id}
                                        name={region.name}
                                    />
                                ))}
                            </Form.Control>
                        </Form.Group>
                    );
                }
            }
        }

        return (
            <Panel>
                {panelContent}
            </Panel>
        );
    }
}

export default Regions;
