import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions as regionsActions } from '../../ducks/regions';
import Regions from './Regions';

class RegionsContainer extends Component {
    componentDidMount() {
        this.props.fetchRegions();
    }

    render() {
        return (
            <Regions {...this.props.regions} />
        );
    }
}

const mapStateToProps = (state) => ({
    regions: state.regions,
});

const mapDispatchToProps = (dispatch) => ({
    fetchRegions: () => dispatch(regionsActions.fetchRegions()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegionsContainer);