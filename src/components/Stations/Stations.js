import React, { Component } from 'react';

import Panel from '../Panel/Panel';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import _ from 'lodash';
import moment from 'moment';
// import Chart from "chart.js";
// import classes from "./LineGraph.module.css";
import {XYPlot, XAxis, YAxis, HorizontalGridLines, VerticalGridLines, ChartLabel, MarkSeries, Hint} from 'react-vis';


const Station = ({ panelContentLeft, panelContentRight }) => (
    <Form>
        {/*<Row>*/}
            <Col>
                {panelContentLeft}
            </Col>
            <Col>
                {panelContentRight}
            </Col>
        {/*</Row>*/}
    </Form>
    // <Form.Check type='checkbox' id={stationId} label={name} />
);

const StationCol  = ({ stationId, name }) => (
    <Form.Check type='checkbox' id={stationId} label={name} />
);

const GraphCol = ({dataset}) => (
    <XYPlot
        xType="time"
        width={1000}
        height={100}>
        <HorizontalGridLines />
        <VerticalGridLines />
        <MarkSeries
            className="mark-series-example"
            strokeWidth={2}
            opacity="0.8"
            className="custom-marking"
            customComponent="square"
            data={dataset}/>
        <XAxis />
        <YAxis tickValues={[1]}/>
        <ChartLabel
            text="Y Axis"
            className="alt-y-label"
            includeMargin={false}
            xPercent={0}
            yPercent={0}
            style={{
                textAnchor: 'end'
            }}
        />
    </XYPlot>
);

class Stations extends Component {

    render() {
        let panelContent, panelContentLeft, panelContentRight;


        if (this.props.loading) {
            panelContentLeft = (
                <p>Loading...</p>
            );
        } else {
            if (this.props.error) {
                panelContentLeft = (
                    <p>Something went wrong while fetching the data: <code>{this.props.error}</code></p>
                );
            } else {
                if(this.props.data.station !== undefined){
                    panelContentLeft = (

                        <Form.Group controlId="stationForm.ControlSelect4">
                            <Form.Label>Stations:</Form.Label>
                            {this.props.data.station.map((station, index) => (
                                <StationCol
                                    key={index}
                                    id={station.id}
                                    name={station.name}
                                />
                            ))}
                        </Form.Group>
                    );
                }
            }
        }

        if (this.props.loadingValues) {
            console.log('loading values...')
        } else {
            if (this.props.error) {
                console.log('error loading values')
            } else {
                if(this.props.dataValues.values[0] !== undefined){
                    // historical average
                    let average = _.meanBy(this.props.dataValues.values[0].values, (p) => p.value);
                    console.log('average',average);

                    let tempValues =[];

                    // find values greater than historical average
                    _.forEach(this.props.dataValues.values[0].values, (o) => {
                        if(o.value > average){
                            tempValues.push(o);
                        }
                    });

                    // group by day
                    let groups = _.groupBy(tempValues, function (o) {
                        return moment(o.timestamp).startOf('day').format();
                    });


                    let minDate = _.min(Object.keys(groups));
                    let maxDate = _.max(Object.keys(groups));
                    const MSEC_DAILY = 86400000;
                    // let data = [{x: 1, y: 10},{x: 2, y: 5},{x: 3, y: 15}];
                    let data =  [];
                    _.forEach(groups, (o, index) => {
                        console.log('foormating',moment(index));
                        let timestamp = new Date(index).getTime();
                        data.push({x:timestamp + MSEC_DAILY, y:10});
                    })
                    console.log(data);
                    panelContentRight = ( <GraphCol
                        dataset={data}

                    />)
                }
            }
        }

        panelContent = (<Station
            panelContentLeft = {panelContentLeft}
            panelContentRight = {panelContentRight}
                        />)

        return (
            <Panel>
                {panelContent}
            </Panel>
        );
    }
}

export default Stations;