import React, { Component } from 'react';
import { connect } from 'react-redux';

import { actions as stationsActions } from '../../ducks/stations';
import Stations from './Stations';

class StationsContainer extends Component {
    componentDidMount() {
        this.props.fetchStations();
        this.props.fetchStationValues();
    }

    render() {
        return (
            <Stations {...this.props.stations} />
        );
    }
}

const mapStateToProps = (state) => ({
    stations: state.stations,
    values: state.values,
});

const mapDispatchToProps = (dispatch) => ({
    fetchStations: () => dispatch(stationsActions.fetchStations()),
    fetchStationValues: () => dispatch(stationsActions.fetchStationValues()),
});

export default connect(mapStateToProps, mapDispatchToProps)(StationsContainer);