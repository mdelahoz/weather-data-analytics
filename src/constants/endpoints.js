let API_HOST = 'http://localhost:4000';

export { API_HOST };
export const GET_AGENCY_URL = `${API_HOST}/weather/v1/agency`;
export const GET_REGION_URL = `${API_HOST}/weather/v1/agency/:id/region`;
export const GET_STATION_URL = `${API_HOST}/weather/v1/station/:id`;
export const GET_METRICS_URL = `${API_HOST}/weather/v1/metrics`;
export const GET_VALUES_URL = `${API_HOST}/weather/v1/metric/:id/values/`;

