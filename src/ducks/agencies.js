import {GET_AGENCY_URL} from "../constants/endpoints";
import axios from 'axios';

export const types = {
    FETCH_AGENCIES_REQUEST: 'FETCH_AGENCIES_REQUEST',
    FETCH_AGENCIES_SUCCESS: 'FETCH_AGENCIES_SUCCESS',
    FETCH_AGENCIES_FAILURE: 'FETCH_AGENCIES_FAILURE',
};

const DEFAULT_STATE = {
    loading: false,
    data: [],
    error: null,
};

export function reducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case types.FETCH_AGENCIES_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case types.FETCH_AGENCIES_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload,
            };
        case types.FETCH_AGENCIES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        default:
            return state;
    }
}

export const actions = {
    fetchAgencies() {
        return function (dispatch, getState) {
            dispatch({
                type: types.FETCH_AGENCIES_REQUEST,
            });

            return axios(GET_AGENCY_URL)
                .then(function (response) {
                    dispatch({
                        type: types.FETCH_AGENCIES_SUCCESS,
                        payload: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: types.FETCH_AGENCIES_FAILURE,
                        payload: err.message,
                    });
                });
        };
    },
};
