import { GET_METRICS_URL } from '../constants/endpoints';
import axios from 'axios'

// actions
export const types = {
    FETCH_METRICS_REQUEST: 'FETCH_METRICS_REQUEST',
    FETCH_METRICS_SUCCESS: 'FETCH_METRICS_SUCCESS',
    FETCH_METRICS_FAILURE: 'FETCH_METRICS_FAILURE',
};

const DEFAULT_STATE = {
    loading: false,
    data: [],
    error: null,
};

// reducer
export function reducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case types.FETCH_METRICS_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case types.FETCH_METRICS_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload,
            };
        case types.FETCH_METRICS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        default:
            return state;
    }
}

//actions
export const actions = {
    fetchMetrics() {
        return function (dispatch, getState) {
            dispatch({
                type: types.FETCH_METRICS_REQUEST
            });

            return axios(GET_METRICS_URL)
                .then(function (response) {
                    dispatch({
                        type: types.FETCH_METRICS_SUCCESS,
                        payload: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: types.FETCH_METRICS_FAILURE,
                        payload: err.message,
                    });
                });
        };
    },
};