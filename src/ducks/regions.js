import { GET_REGION_URL } from "../constants/endpoints";
import axios from 'axios';

// actions
export const types = {
    FETCH_REGION_REQUEST: 'FETCH_REGION_REQUEST',
    FETCH_REGION_SUCCESS: 'FETCH_REGION_SUCCESS',
    FETCH_REGION_FAILURE: 'FETCH_REGION_FAILURE',
};

const DEFAULT_STATE = {
    loading: false,
    data: [],
    error: null,
};

// reducer
export function reducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case types.FETCH_REGION_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case types.FETCH_REGION_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload,
            };
        case types.FETCH_REGION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        default:
            return state;
    }
}

//actions
export const actions = {
    fetchRegions(agencyId) {
        console.log('fetching regions');
        console.log(agencyId);
        return function (dispatch, getState) {
            dispatch({
                type: types.FETCH_REGION_REQUEST
            });
            // const url = GET_REGION_URL.replace(':id',agencyId);
            return axios(GET_REGION_URL)
                .then(function (response) {
                    dispatch({
                        type: types.FETCH_REGION_SUCCESS,
                        payload: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: types.FETCH_REGION_FAILURE,
                        payload: err.message,
                    });
                });
        };
    },
};