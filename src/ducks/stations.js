import {GET_STATION_URL, GET_VALUES_URL} from '../constants/endpoints';
import axios from 'axios';

// actions
export const types = {
    FETCH_STATION_REQUEST: 'FETCH_STATION_REQUEST',
    FETCH_STATION_SUCCESS: 'FETCH_STATION_SUCCESS',
    FETCH_STATION_FAILURE: 'FETCH_STATION_FAILURE',
    FETCH_STATION_VALUES_REQUEST: 'FETCH_STATION_VALUES_REQUEST',
    FETCH_STATION_VALUES_SUCCESS: 'FETCH_STATION_VALUES_SUCCESS',
    FETCH_STATION_VALUES_FAILURE: 'FETCH_STATION_VALUES_FAILURE',
};

const DEFAULT_STATE = {
    loading: false,
    data: [],
    error: null,
    loadingValues: false,
    dataValues: [],
    errorValues: null,
};

// reducer
export function reducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case types.FETCH_STATION_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case types.FETCH_STATION_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload,
            };
        case types.FETCH_STATION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        case types.FETCH_STATION_VALUES_REQUEST:
            return {
                ...state,
                loadingValues: true,
            };
        case types.FETCH_STATION_VALUES_SUCCESS:
            return {
                ...state,
                loadingValues: false,
                dataValues: action.payload,
            };
        case types.FETCH_STATION_VALUES_FAILURE:
            return {
                ...state,
                loadingValues: false,
                errorValues: action.payload,
            };
        default:
            return state;
    }
}

//actions
export const actions = {
    fetchStations() {
        return function (dispatch, getState) {
            dispatch({
                type: types.FETCH_STATION_REQUEST
            });

            return axios(GET_STATION_URL)
                .then(function (response) {
                    console.log('duck fetch', response);
                    dispatch({
                        type: types.FETCH_STATION_SUCCESS,
                        payload: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: types.FETCH_STATION_FAILURE,
                        payload: err.message,
                    });
                });
        };
    },
    fetchStationValues() {
        return function (dispatch, getState) {
            dispatch({
                type: types.FETCH_STATION_VALUES_REQUEST
            });

            return axios(GET_VALUES_URL)
                .then(function (response) {
                    console.log('duck fetch metrics', response);
                    dispatch({
                        type: types.FETCH_STATION_VALUES_SUCCESS,
                        payload: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: types.FETCH_STATION_VALUES_FAILURE,
                        payload: err.message,
                    });
                });
        };
    },
};