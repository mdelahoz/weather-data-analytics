//action types
export const types = {
    GET_AGENCY: 'GET_AGENCY',
    GET_REGION: 'GET_REGION',
    GET_METRICS: 'GET_METRICS',
    GET_STATION: 'GET_STATION',
};

const DEFAULT_STATE = {
    agency: [],
    aLoaded: false,
    region: [],
    rLoaded: false,
    metrics: [],
    mLoaded: false,
    station: [],
    sLoaded: false,
};

//reducer
export function reducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case types.GET_AGENCY:
            return {
                ...state,
                agency: action.payload,
            };
        case types.GET_REGION:
            return {
                ...state,
                region: {
                    ...state,
                    region: action.payload,
                },
            };
        case types.GET_METRICS:
            return {
                ...state,
                metrics: action.payload,
            };
        case types.GET_STATION:
            return {
                ...state,
                region: {
                    ...state,
                    station: action.payload,
                },
            };
        default:
            return state;
    }
}

//action
export const actions = {
    getAgency(agency) {
        return {
            type: types.GET_AGENCY,
            payload: agency,
        };
    },
    getRegion(region) {
        return {
            type: types.GET_REGION,
            payload: region,
        };
    },
    getMetrics(metrics) {
        return {
            type: types.GET_METRICS,
            payload: metrics,
        };
    },
    getStation(station) {
        return {
            type: types.GET_STATION,
            payload: station,
        };
    },

};
