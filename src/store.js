import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { reducer as weather } from './ducks/weather';
import { reducer as agencies } from './ducks/agencies';
import { reducer as metrics } from './ducks/metrics';
import { reducer as regions } from './ducks/regions';
import { reducer as stations } from './ducks/stations';


const rootReducer = combineReducers({
    weather,
    agencies,
    metrics,
    regions,
    stations,
});

export default createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk)
));